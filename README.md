# Spring Cloud Eureka Registry Server #

##Multiple instances##
Edit your computer's /etc/hosts file (c:\WINDOWS\system32\drivers\etc\hosts on Windows).
Add the following lines and save your work:
127.0.0.1       eureka-primary
127.0.0.1       eureka-secondary
127.0.0.1       eureka-tertiary

Run the application three times, using -Dspring.profiles.active=primary (and secondary, and tertiary) to activate the relevant profile. The result should be three Eureka servers which communicate with each other.